//
//  RecipeListViewController.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import UIKit

class MovieListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var model = MovieModel()
    var selectedType: MovieType?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetail" {
            if let indexPath = tableView.indexPathForSelectedRow{
                let movie = model.movies[indexPath.row]
                let detailVC = segue.destination as? MovieDetailViewController
                detailVC?.movie = movie
            }
        }
        else if segue.identifier == "addMovie"{
            let navVC = segue.destination as? UINavigationController
            let addVC = navVC?.viewControllers.first as? AddMovieViewController
            addVC?.delegate = self
        }
    }
    

    @IBAction func didChangeFilter(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            selectedType = nil
        case 1:
            selectedType = .action
        case 2:
            selectedType = .scifi
        case 3:
            selectedType = .comedy
        default:
            break
        }
        
        tableView.reloadData()
    }
}
    
extension MovieListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.movies(forType: selectedType).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BurgerCell", for: indexPath )
        let movie = model.movies(forType: selectedType)[indexPath.row]
        
        cell.textLabel?.text = movie.name
        cell.detailTextLabel?.text = movie.cast
        cell.imageView?.image = UIImage(named: movie.thumbnailName)
        
        return cell
    }
}

extension MovieListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension MovieListViewController: AddMovieDelegate{
    func add(movie: Movie){
        model.add(movie: movie)
        tableView.reloadData()
    }
}
