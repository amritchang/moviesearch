//
//  RecipeDetailViewController.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var castsTextView: UITextView!
    
    var movie: Movie!
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerImageView.image = UIImage(named: movie.imageName)
        nameLabel.text = movie.name
        castsTextView.text = bulletedList(forCasts: movie.cast)
        // Do any additional setup after loading the view.
    }
    
    private func bulletedList(forCasts casts: String) -> String {
        var list = ""
        let items = casts.split(separator: ",")
        items.forEach{list.append("\u{2022} \($0) \n")}
        return list
    }

  

}
