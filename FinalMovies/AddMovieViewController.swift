//
//  AddRecipeViewController.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import UIKit

protocol AddMovieDelegate {
    func add(movie: Movie)
}

class AddMovieViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var castsTextView: UITextView!
    
    let castsPlaveholder = "Add cast"
    
    var delegate: AddMovieDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        castsTextView.text = castsPlaveholder
        castsTextView.delegate = self
        // Do any additional setup after loading the view.
    }
    

    @IBAction func didTapCancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        guard let name = nameTextField.text else {return}
        let newMovie = Movie(name: name, cast: castsTextView.text, imageName: "", thumbnailName: "", type: .action)
        delegate?.add(movie: newMovie)
        dismiss(animated: true)
    }
}

extension AddMovieViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.tertiaryLabel ){
            textView.text = nil
            textView.textColor = UIColor.label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty){
            textView.text = castsPlaveholder
            textView.textColor = UIColor.tertiaryLabel
        }
    }
}
